﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Data.Entity;

namespace JakToToTo
{
    public partial class Form1 : Form
    {
        ObslugaListy listy = new ObslugaListy();
        JakToToTo_db Nasza_Baza = new JakToToTo_db();
        ObslugaPogody obp = new ObslugaPogody();

        public Form1()
        {
            InitializeComponent();
        }
        
        //WYKONAJ WSZYSTKO POGODA I ZADANIA
        private void button2_Click(object sender, EventArgs e)
        {
            //listy.wykonajWszystko();
            listy.wykonajWszystkozBazyDanych();
            obp.wykonajWszystkozBazyDanych();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string x = czegoSzukam.Text;
        }
        
        //Dodawanie do listy ZADANIA
        private void button3_Click(object sender, EventArgs e)
        {
            listy.dodajZadanie(czegoSzukam.Text, mailOdbiorcy.Text, "http://" + strona.Text);
            listaZadan.Items.Add(listy.oddajZadanie(listy.zadania.Count-1));
            
        }
        //Dodawanie do listy POGODA
        private void button4_Click(object sender, EventArgs e)
        {
            obp.dodajZadanie(mailOdbiorcy.Text, textBox1.Text);
            listaZadan.Items.Add(obp.oddajZadanie(obp.zadania.Count - 1));
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }
        private void textBox3_TextChanged_1(object sender, EventArgs e)
        {

        }
        private void label2_Click(object sender, EventArgs e)
        {

        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            
        }

        //Czysc Baze Danych POGODA I ZADANIA
        private void czysc_Click(object sender, EventArgs e)
        {
            listy.zadania.Clear();
            listaZadan.Items.Clear();
            obp.zadania.Clear();

            using (var ctx = new JakToToTo_db())
            {
                foreach (var g in ctx.ZadanieDb2)
                {
                    ctx.ZadanieDb2.Remove(g);
                }

                foreach (var g in ctx.PogodaDb2)
                {
                    ctx.PogodaDb2.Remove(g);
                }
                ctx.SaveChanges();
            }
            
        }

        private void serializacja_Click(object sender, EventArgs e)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(Application.StartupPath + @"\serializacja.txt"))
            {
                for (int i = 0; i < listy.zadania.Count; i++)
                {
                    file.WriteLine(listy.oddajZadanie(i));
                }
            }
        }
        private void deserializacja_Click(object sender, EventArgs e)
        {
            string[] linie = File.ReadAllLines(Application.StartupPath + @"\serializacja.txt");
            foreach (string linia in linie)
            {
                String[] slowo=linia.Split(' ');
                listy.dodajZadanie(slowo[0], slowo[1],slowo[2]);
                listaZadan.Items.Add(listy.oddajZadanie(listy.zadania.Count - 1));
            }
        }
        private void listaZadan_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        //Wczytywanie z Bazy do listy POGODY I ZADAN
        private void button1_Click_1(object sender, EventArgs e)
        {
            using (var ctx = new JakToToTo_db())
            {
                foreach (var g in ctx.ZadanieDb2)
                {
                    listaZadan.Items.Add(g.ID+" "+g.srcBaza+" "+g.mailBaza+" "+g.stronaBaza);
                }
                foreach (var g in ctx.PogodaDb2)
                {
                    listaZadan.Items.Add(g.PogodaBazaID + " " + g.mailBaza +" "+ g.pogodaBaza);
                }

            }
        }

        private void SprawdzPogode_Click(object sender, EventArgs e)
        {
            string[] a = obp.NowaPogoda(NazwaMiasta.Text);
            string cos = "Aktualna Temperatura : " + a[0];
            string iconurl = "http://openweathermap.org/img/w/" + a[1] + ".png";
            ZdjeciePogody.Load(iconurl);
            textBox1.Text = cos;
        }

        private void SprawdzIWyslij_Click(object sender, EventArgs e)
        {
            string[] a = obp.NowaPogoda(NazwaMiasta.Text);
            string AktualneTemp = "Aktualna Temperatura : " + a[0];
            if (TemperaturaZadana.Value < System.Convert.ToInt32(a[0]))
            {
                AktualneTemp = AktualneTemp + ", wyslano";
            }
            textBox1.Text = AktualneTemp;
            string iconurl = "http://openweathermap.org/img/w/" + a[1] + ".png";
            ZdjeciePogody.Load(iconurl);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
