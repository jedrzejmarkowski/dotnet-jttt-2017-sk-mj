namespace JakToToTo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Zadanies",
                c => new
                    {
                        ZadanieID = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.ZadanieID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Zadanies");
        }
    }
}
