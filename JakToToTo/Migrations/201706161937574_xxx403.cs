namespace JakToToTo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class xxx403 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ZadanieBazas", "ListaZadanBaza_ID", "dbo.ListaZadanBazas");
            DropIndex("dbo.ZadanieBazas", new[] { "ListaZadanBaza_ID" });
            DropColumn("dbo.ZadanieBazas", "ListaZadanBaza_ID");
            DropTable("dbo.ListaZadanBazas");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ListaZadanBazas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.ZadanieBazas", "ListaZadanBaza_ID", c => c.Int());
            CreateIndex("dbo.ZadanieBazas", "ListaZadanBaza_ID");
            AddForeignKey("dbo.ZadanieBazas", "ListaZadanBaza_ID", "dbo.ListaZadanBazas", "ID");
        }
    }
}
