namespace JakToToTo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class xxx1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ZadanieBazas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        srcBaza = c.String(),
                        mailBaza = c.String(),
                        stronaBaza = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ZadanieBazas");
        }
    }
}
