namespace JakToToTo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class sdd : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PogodaBazas",
                c => new
                    {
                        PogodaBazaID = c.Int(nullable: false, identity: true),
                        mailBaza = c.String(),
                        pogodaBaza = c.String(),
                    })
                .PrimaryKey(t => t.PogodaBazaID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PogodaBazas");
        }
    }
}
