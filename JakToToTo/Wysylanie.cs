﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Mime;
using System.Net;
namespace JakToToTo
{
    class Wysylanie
    {
        string MojMail = "konto.do.wysylania.si.szarp@gmail.com";
        string Haslo = "Politechnika";
        string KogosMail = "";
        
        public void wyslij(String scrImage)
        {
            MailMessage Wiadomość = new MailMessage();
            Wiadomość.From = new MailAddress(MojMail);
            Wiadomość.To.Add(KogosMail);
            Wiadomość.Subject = "Obrazek-Demotywatory";
            Wiadomość.AlternateViews.Add(getEmbeddedImage(scrImage));
            Wiadomość.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Credentials = new NetworkCredential(MojMail, Haslo);
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.Send(Wiadomość);
        }

        public void wyslijPogode(String WiadoPogoda)//nazwa miejsca w tytule???
        {
            MailMessage Wiadomość = new MailMessage();
            Wiadomość.From = new MailAddress(MojMail);
            Wiadomość.To.Add(KogosMail);
            Wiadomość.Subject = "Obrazek-Pogoda";
            Wiadomość.Body = WiadoPogoda;
            Wiadomość.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Credentials = new NetworkCredential(MojMail, Haslo);
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.Send(Wiadomość);
        }

        private AlternateView getEmbeddedImage(String filePath)
        {
            LinkedResource inline = new LinkedResource(filePath);
            inline.ContentId = Guid.NewGuid().ToString();
            string htmlBody = @"<img src='cid:" + inline.ContentId + @"'/>";
            AlternateView alternateView = AlternateView.CreateAlternateViewFromString(htmlBody, null, MediaTypeNames.Text.Html);
            alternateView.LinkedResources.Add(inline);
            return alternateView;
        }

        public void SetKogosMail(string maill)
        {
            KogosMail = maill;
        }
    }
}
