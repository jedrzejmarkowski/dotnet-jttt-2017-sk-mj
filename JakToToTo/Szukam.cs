﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using HtmlAgilityPack;
using System.IO;

namespace JakToToTo
{
    class Szukam
    {
        public string _url = "";
        public string Poszukiwany_Src;//adres pliku ze zdjeciem
        public List<string> tab_src2 = new List<string>();
        public List<string> tab_alt2 = new List<string>();
        
        public string GetPageHtml(String sciezka)
        {
            using (var wc = new WebClient())
            {
                try {
                    wc.Encoding = Encoding.UTF8;
                    var html = System.Net.WebUtility.HtmlDecode(wc.DownloadString(_url));
                    return html;
                }
                catch
                {
                    using (StreamWriter sw = new StreamWriter(sciezka + @"\log.txt", true))
                    {
                        sw.WriteLine("Nieprawidlowy url strony "+_url);
                    }
                    return "";
                }
                
            }
        }
        
        public void SetUrl(string strona)
        {
            _url = strona;
            Console.Write(_url);
        }

        public void PrintPageNodes(String sciezka)
        {
            var doc = new HtmlDocument();
            var pageHtml = GetPageHtml(sciezka);
            doc.LoadHtml(pageHtml);
            var nodes = doc.DocumentNode.Descendants("img");
            
            foreach (var node in nodes)
            {
                tab_src2.Add(node.GetAttributeValue("src", ""));
                tab_alt2.Add(node.GetAttributeValue("alt", ""));
             }
        }

        public string Czy_Zawieram(string haslo)
        {
            for(int i = 0; i < tab_src2.Count; i++)
            {
                if (tab_alt2[i].Contains(haslo))
                {
                    Poszukiwany_Src = tab_src2[i];
                }
            }
            return Poszukiwany_Src;
        }

        

    }
}
