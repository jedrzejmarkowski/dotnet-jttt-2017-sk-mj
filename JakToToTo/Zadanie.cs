﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JakToToTo
{
    class Zadanie
    {

        public int ZadanieID { get; set; }

        public String Src="";
        public String Adres_Mail="";
        public String Adres_Strony="";

        public Zadanie(int ID, String source, String email, String web)
        {
            ZadanieID = ID;
            Adres_Mail = email;
            Src = source;
            Adres_Strony = web;
        }

        public String Get_Src()
        {
            return Src;
        }
        public String Get_Mail()
        {
            return Adres_Mail;
        }
        public String Get_Strona()
        {
            return Adres_Strony;
        }


    }
}
