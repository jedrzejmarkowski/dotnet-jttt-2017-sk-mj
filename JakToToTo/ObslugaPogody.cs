﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Dynamic;
using System.IO;
using System.Windows.Forms;

namespace JakToToTo
{
    class ObslugaPogody
    {
        Wysylanie Wiadomosc = new Wysylanie();
        private string url1 = "http://api.openweathermap.org/data/2.5/weather?q=";
        private string url2 = "&APPID=6066c1bee0f825a7ef7ee5ca24e31177";
        int ktorezadanie=1;

        public List<Pogoda> zadania = new List<Pogoda>();
        JakToToTo_db Nasza_Baza = new JakToToTo_db();

        public string[] NowaPogoda(string nazwa_miasta)//zwraca temp[0]=temperature i temp[1]=id obrazka 
        {
            string calosc = url1 + nazwa_miasta + url2;
            var json = new WebClient().DownloadString(calosc);
            Pogoda pog = JsonConvert.DeserializeObject<Pogoda>(json);
            string[] temp = new string[2];
            temp[0] = System.Convert.ToString(pog.main.temp_max - 273);
            temp[1] = pog.weather[0].icon;
            return temp;
        }

        public void wykonajWszystkozBazyDanych()
        {
            using (var ctx = new JakToToTo_db())
            {
                foreach (var g in ctx.PogodaDb2)
                {
                    Wiadomosc.SetKogosMail(g.mailBaza);
                    Wiadomosc.wyslijPogode(g.pogodaBaza);
                    using (StreamWriter sw = new StreamWriter(Application.StartupPath + @"\log.txt", true))
                    {
                        sw.WriteLine("Wyslano pomyslnie " + g.PogodaBazaID + " ze strony " + g.pogodaBaza + " na maila " + g.mailBaza);
                    }
                }
            }
        }

        public void dodajZadanie(String email, String wiadomocPogody)
        {
            Pogoda zadPogoda = new Pogoda(ktorezadanie, email, wiadomocPogody);
            
            zadania.Add(zadPogoda);
            PogodaBaza zad2;
            zad2 = new PogodaBaza() { mailBaza = email, pogodaBaza = wiadomocPogody, PogodaBazaID = ktorezadanie };

            Nasza_Baza.PogodaDb2.Add(zad2);
            Nasza_Baza.SaveChanges();
            ktorezadanie++;
        }
        public String oddajZadanie(int i)
        {
            String s = "";
            s = zadania[i].mailBaza + " " + zadania[i].pogodaBaza +" "+ zadania[i].PogodaBazaID;
            return s;
        }

    }
}
