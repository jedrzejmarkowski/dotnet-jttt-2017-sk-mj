﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JakToToTo
{
    class ObslugaListy
    {
        Szukam Szuk = new Szukam();
        Wysylanie Wiadomosc = new Wysylanie();
        public int ktorezadanie = 1;
        JakToToTo_db Nasza_Baza = new JakToToTo_db();
        public List<Zadanie> zadania = new List<Zadanie>();
        
        public void dodajZadanie(String src, String email, String web)
        {
            Zadanie zad = new Zadanie(ktorezadanie,src, email, web);
            zadania.Add(zad);

            ZadanieBaza zad2;
            zad2 = new ZadanieBaza() { srcBaza=src,mailBaza=email,stronaBaza=web, ID = ktorezadanie};
            Nasza_Baza.ZadanieDb2.Add(zad2);
            Nasza_Baza.SaveChanges();
            ktorezadanie++;
        }
        
        public String oddajZadanie(int i)
        {
            String s = "";
            s = zadania[i].Src + " " + zadania[i].Adres_Mail + " " + zadania[i].Adres_Strony;
            return s;
        }
       

        public String GetSRC(int i)
        {
            String s = "";
            s = zadania[i].Src;
            return s;
        }
        public String GetMAIL(int i)
        {
            String s = "";
            s = zadania[i].Adres_Mail ;
            return s;
        }
        public String GetSTRONA(int i)
        {
            String s = "";
            s = zadania[i].Adres_Strony;
            return s;
        }


        public void wykonajWszystkozBazyDanych()
        {
            using (var ctx = new JakToToTo_db())
            {
                foreach (var g in ctx.ZadanieDb2)
                {
                    string src;
                    Szuk.SetUrl(g.stronaBaza);
                    Szuk.PrintPageNodes(Application.StartupPath);
                    string localpath = "";
                    src = Szuk.Czy_Zawieram(g.srcBaza);
                    if (src == null)
                    {
                        using (StreamWriter sw = new StreamWriter(Application.StartupPath + @"\log.txt", true))
                        {
                            sw.WriteLine("Nie ma takiego elementu: " + g.srcBaza + " na stronie " + g.srcBaza);
                        }
                    }else{

                        localpath = Application.StartupPath + @"\" + g.srcBaza + ".jpg";//plik bedzie zapisany pod nazwa image
                        using (WebClient client = new WebClient())
                        {
                            if (!File.Exists(localpath))
                            {
                                client.DownloadFile(src, localpath);//Pobierze plik z src i zapisze w localpath
                            }
                        }
                        Wiadomosc.SetKogosMail(g.mailBaza);
                        Wiadomosc.wyslij(Application.StartupPath + @"\" + g.srcBaza + ".jpg");
                        using (StreamWriter sw = new StreamWriter(Application.StartupPath + @"\log.txt", true))
                        {
                            sw.WriteLine("Wyslano pomyslnie " + g.srcBaza + " ze strony " + g.stronaBaza + " na maila " + g.mailBaza);
                        }
                    }
                }
            }
        }
        
        public void wykonajWszystko()
        {
            for(int i = 0; i < zadania.Count;i++)
            {
                string src;
                Szuk.SetUrl(zadania[i].Adres_Strony);
                Szuk.PrintPageNodes(Application.StartupPath);
                string localpath = "";
                src = Szuk.Czy_Zawieram(zadania[i].Src);
                if (src == null) {
                    using (StreamWriter sw = new StreamWriter(Application.StartupPath + @"\log.txt", true))
                    {
                        sw.WriteLine("Nie ma takiego elementu: " + zadania[i].Src + " na stronie " + zadania[i].Adres_Strony );
                    }
                }
                else {
                    localpath = Application.StartupPath + @"\" + zadania[i].Src + ".jpg";//plik bedzie zapisany pod nazwa image
                    using (WebClient client = new WebClient())
                    {
                        if (!File.Exists(localpath))
                        {
                            client.DownloadFile(src, localpath);//Pobierze plik z src i zapisze w localpath
                        }
                    }
                        Wiadomosc.SetKogosMail(zadania[i].Adres_Mail);
                        Wiadomosc.wyslij(Application.StartupPath + @"\" + zadania[i].Src + ".jpg");
                    using (StreamWriter sw = new StreamWriter(Application.StartupPath + @"\log.txt", true))
                    {
                        sw.WriteLine("Wyslano pomyslnie " + zadania[i].Src + " ze strony " + zadania[i].Adres_Strony + " na maila " + zadania[i].Adres_Mail);
                    }
                }
            }
            

        }

    }
}
