﻿namespace JakToToTo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button2 = new System.Windows.Forms.Button();
            this.czegoSzukam = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.mailOdbiorcy = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.strona = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.listaZadan = new System.Windows.Forms.ListBox();
            this.czysc = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.ZdjeciePogody = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TemperaturaZadana = new System.Windows.Forms.NumericUpDown();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SprawdzIWyslij = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.NazwaMiasta = new System.Windows.Forms.TextBox();
            this.SprawdzPogode = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ZdjeciePogody)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TemperaturaZadana)).BeginInit();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(843, 373);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(221, 95);
            this.button2.TabIndex = 2;
            this.button2.Text = "Wykonaj";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // czegoSzukam
            // 
            this.czegoSzukam.Location = new System.Drawing.Point(92, 117);
            this.czegoSzukam.Name = "czegoSzukam";
            this.czegoSzukam.Size = new System.Drawing.Size(285, 20);
            this.czegoSzukam.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(89, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Czego chcesz szukać?";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(92, 237);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(285, 36);
            this.button3.TabIndex = 7;
            this.button3.Text = "Dodaj do listy";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(58, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "1";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(58, 120);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(58, 194);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "3";
            // 
            // mailOdbiorcy
            // 
            this.mailOdbiorcy.Location = new System.Drawing.Point(92, 191);
            this.mailOdbiorcy.Name = "mailOdbiorcy";
            this.mailOdbiorcy.Size = new System.Drawing.Size(285, 20);
            this.mailOdbiorcy.TabIndex = 14;
            this.mailOdbiorcy.TextChanged += new System.EventHandler(this.textBox3_TextChanged_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(89, 159);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Gdzie wysyłasz?";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // strona
            // 
            this.strona.Location = new System.Drawing.Point(92, 54);
            this.strona.Name = "strona";
            this.strona.Size = new System.Drawing.Size(285, 20);
            this.strona.TabIndex = 16;
            this.strona.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(89, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "Gdzie szukać?";
            // 
            // listaZadan
            // 
            this.listaZadan.FormattingEnabled = true;
            this.listaZadan.Location = new System.Drawing.Point(843, 25);
            this.listaZadan.Name = "listaZadan";
            this.listaZadan.Size = new System.Drawing.Size(435, 342);
            this.listaZadan.TabIndex = 19;
            this.listaZadan.SelectedIndexChanged += new System.EventHandler(this.listaZadan_SelectedIndexChanged);
            // 
            // czysc
            // 
            this.czysc.Location = new System.Drawing.Point(1070, 373);
            this.czysc.Name = "czysc";
            this.czysc.Size = new System.Drawing.Size(208, 46);
            this.czysc.TabIndex = 20;
            this.czysc.Text = "Czysc";
            this.czysc.UseVisualStyleBackColor = true;
            this.czysc.Click += new System.EventHandler(this.czysc_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1070, 422);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(208, 46);
            this.button1.TabIndex = 21;
            this.button1.Text = "Wczytaj";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // ZdjeciePogody
            // 
            this.ZdjeciePogody.Location = new System.Drawing.Point(237, 489);
            this.ZdjeciePogody.Name = "ZdjeciePogody";
            this.ZdjeciePogody.Size = new System.Drawing.Size(140, 62);
            this.ZdjeciePogody.TabIndex = 37;
            this.ZdjeciePogody.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(88, 377);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(185, 13);
            this.label8.TabIndex = 36;
            this.label8.Text = "Jeśli pogoda wyższa niż podana wyślij";
            // 
            // TemperaturaZadana
            // 
            this.TemperaturaZadana.Location = new System.Drawing.Point(91, 403);
            this.TemperaturaZadana.Name = "TemperaturaZadana";
            this.TemperaturaZadana.Size = new System.Drawing.Size(120, 20);
            this.TemperaturaZadana.TabIndex = 35;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(92, 489);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(127, 62);
            this.textBox1.TabIndex = 34;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // SprawdzIWyslij
            // 
            this.SprawdzIWyslij.Location = new System.Drawing.Point(186, 445);
            this.SprawdzIWyslij.Name = "SprawdzIWyslij";
            this.SprawdzIWyslij.Size = new System.Drawing.Size(191, 23);
            this.SprawdzIWyslij.TabIndex = 33;
            this.SprawdzIWyslij.Text = "Sprawdz i wyślij";
            this.SprawdzIWyslij.UseVisualStyleBackColor = true;
            this.SprawdzIWyslij.Click += new System.EventHandler(this.SprawdzIWyslij_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(89, 311);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(177, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "Sprawdzenie Pogody (Podaj Miasto)";
            // 
            // NazwaMiasta
            // 
            this.NazwaMiasta.Location = new System.Drawing.Point(92, 340);
            this.NazwaMiasta.Name = "NazwaMiasta";
            this.NazwaMiasta.Size = new System.Drawing.Size(285, 20);
            this.NazwaMiasta.TabIndex = 31;
            // 
            // SprawdzPogode
            // 
            this.SprawdzPogode.Location = new System.Drawing.Point(92, 445);
            this.SprawdzPogode.Name = "SprawdzPogode";
            this.SprawdzPogode.Size = new System.Drawing.Size(75, 23);
            this.SprawdzPogode.TabIndex = 30;
            this.SprawdzPogode.Text = "Sprawdz";
            this.SprawdzPogode.UseVisualStyleBackColor = true;
            this.SprawdzPogode.Click += new System.EventHandler(this.SprawdzPogode_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(91, 565);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(285, 36);
            this.button4.TabIndex = 38;
            this.button4.Text = "Dodaj do listy";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(58, 343);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(13, 13);
            this.label9.TabIndex = 39;
            this.label9.Text = "1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(58, 406);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(13, 13);
            this.label10.TabIndex = 40;
            this.label10.Text = "2";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(58, 577);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(13, 13);
            this.label11.TabIndex = 41;
            this.label11.Text = "3";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Yellow;
            this.ClientSize = new System.Drawing.Size(1320, 613);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.ZdjeciePogody);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.TemperaturaZadana);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.SprawdzIWyslij);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.NazwaMiasta);
            this.Controls.Add(this.SprawdzPogode);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.czysc);
            this.Controls.Add(this.listaZadan);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.strona);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.mailOdbiorcy);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.czegoSzukam);
            this.Controls.Add(this.button2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ZdjeciePogody)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TemperaturaZadana)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox czegoSzukam;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox mailOdbiorcy;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox strona;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ListBox listaZadan;
        private System.Windows.Forms.Button czysc;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox ZdjeciePogody;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown TemperaturaZadana;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button SprawdzIWyslij;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox NazwaMiasta;
        private System.Windows.Forms.Button SprawdzPogode;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
    }
}

